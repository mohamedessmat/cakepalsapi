import type { Config } from '@jest/types';

const config: Config.InitialOptions = {
  verbose: true,
  transform: {
    '^.+\\.ts': 'ts-jest',
  },
  testTimeout: 3000000,
  coverageThreshold: {
    global: {
      functions: 70,
      lines: 70,
    },
  },
};
export default config;
