# CakePalsApi

This is the API for Cake Pals App which is a Cake reservation app where bakers can sell their products and members can buy cakes .

## Running The Server Locally

* First You need to Have `Docker` Installed on your Machine You can follow this tutorial to install Docker [Install Docker](https://docs.docker.com/engine/install/) .

* Navigate To The App DIR and run the following from your terminal :
```bash
docker-compose build
```
this build the docker images .

* Run the following in your temrinal to run the containers

```bash
docker-compose up -d
```
Now the App is up and running at ``Port 3000`` .

You can find a documentation of the API on the following link :
[Documentation](https://documenter.getpostman.com/view/17408319/2s93RXtr9d)

In the Figure Below You Can Find the Architecture Diagram for the API

[![ArchDiagram](./docs/archDiagram.jpeg)](./docs/archDiagram.jpeg)

## Future Recommendations

* Implement Unit Tests for all Models and routes .
* Deploy The API .
* Implement Caching .
* Implement CI/CD pipline for deployment and testing stages .

## Notes

* The `.env` file shouldn't be available at the repo but for the assesment needs i pushed it.
