FROM node:16


WORKDIR /cakepalsapi
COPY package.json .

RUN npm install
RUN npm install pm2 -g
RUN npm install tsc -g
RUN npm install bcrypt
COPY . .
CMD npm run dev
