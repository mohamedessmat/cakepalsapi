import { roles } from '../services/utils/rolesService';
import { RequestInterface as IReq } from '../types/utils.interface';
import { Response, NextFunction } from 'express';
import httpStatus from 'http-status';

export const grantUserAccess = (action: string, resource: string) => {
  return async (req: IReq, res: Response, next: NextFunction): Promise<void> => {
    try {
      const permissions = roles.can(req.user.role)[action](resource);
      if (!permissions.granted) {
        res.status(httpStatus.UNAUTHORIZED);
        res.json({
          message: "You don't have enough permission to perform this action",
        });
        return;
      }
      return next();
    } catch (err) {
      res.status(httpStatus.BAD_REQUEST);
      res.json({
        message: `Something Went Wrong ${err}`,
      });
      return;
    }
  };
};
