import { NextFunction, Request, Response } from 'express';
import httpStatus from 'http-status';
import { ApiError } from '../services/utils/ApiError';
export const validateRequest = (schema: any) => {
  return (req: Request, res: Response, next: NextFunction) => {
    const { error } = schema.validate(req.body);
    if (error) {
      const err = new ApiError(error.details[0].message, httpStatus.BAD_REQUEST);
      next(err);
    }
    next();
    return;
  };
};
