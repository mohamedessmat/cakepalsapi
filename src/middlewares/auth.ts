import { RequestInterface as IReq } from '../types/utils.interface';
import { Response as IRes, NextFunction } from 'express';
import passport from 'passport';
import { IUser } from '../types/user.interface';
import httpStatus from 'http-status';
import jwt from 'jsonwebtoken';
import { JWTInterface } from '../types/utils.interface';
import User from '../models/User';
import Admin from '../models/Admin';
import bcrypt from 'bcrypt';

export const userAuth = async (req: IReq, res: IRes, next: NextFunction): Promise<void> => {
  const authHeader = req.headers.authorization;
  const userID = req.params.id;
  if (authHeader === undefined) {
    res.status(httpStatus.UNAUTHORIZED);
    res.json({
      message: 'Invalid Token',
    });
    return;
  }
  const token = authHeader.split(' ')[1];
  try {
    const decoded = jwt.verify(token, process.env.TOKEN_SECRET_DEV) as JWTInterface;
    if (decoded.userId !== userID) {
      res.status(httpStatus.UNAUTHORIZED);
      res.json({
        message: 'Not Allowed to Perform This Operation',
      });
      return;
    }
    next();
  } catch (err) {
    res.status(httpStatus.BAD_REQUEST);
    res.json({
      message: err.message,
    });
  }
};

export const allowIfLoggedIn = async (req: IReq, res: IRes, next: NextFunction): Promise<void> => {
  try {
    const user = res.locals.loggedInUser;
    if (!user) {
      res.status(httpStatus.UNAUTHORIZED);
      res.json({
        message: 'Please Login and try again',
      });
      return;
    }
    req.user = user;
    next();
    return;
  } catch (err) {
    res.status(httpStatus.BAD_REQUEST);
    res.json(err.message);
    return;
  }
};
export const verifyJWT = async (req: IReq, res: IRes, next: NextFunction): Promise<void> => {
  try {
    const authHeader = req.headers.authorization;
    if (authHeader) {
      const accessToken = authHeader.split(' ')[1];
      const decoded = jwt.verify(accessToken, process.env.TOKEN_SECRET_DEV) as JWTInterface;
      // Check if token has expired
      if (decoded.exp < Date.now().valueOf() / 1000) {
        res.status(401).json({
          error: 'JWT token has expired, please login to obtain a new one',
        });
        return;
      }
      if (decoded.role == 'admin') {
        res.locals.loggedInUser = await Admin.findById(decoded.userId);
        next();
        return;
      }
      res.locals.loggedInUser = await User.findById(decoded.userId);
      next();
    } else {
      next();
    }
  } catch (err) {
    res.status(httpStatus.BAD_REQUEST);
    res.json({
      message: err.message,
    });
  }
};

export const requireLocalAuth = (req: IReq, res: IRes, next: NextFunction): void => {
  passport.authenticate('local', async (err: Error, user: IUser, info: string) => {
    if (err) {
      return next(err);
    }
    if (!user) {
      return res.status(422).send(info);
    }

    req.user = user;
    next();
    return;
  })(req, res, next);
};

export const requireAdminAuth = async (req: IReq, res: IRes, next: NextFunction): Promise<void> => {
  try {
    const { email, password } = req.body;
    const admin = await Admin.findOne({ email: email });
    if (!admin) {
      res.status(httpStatus.UNAUTHORIZED);
      res.json({
        message: 'Email not Exists',
      });
      return;
    }

    bcrypt.compare(password + process.env.BCRYPT_PASSWORD, admin.password, (err, isMatch) => {
      if (err) {
        res.status(httpStatus.UNAUTHORIZED);
        res.json({
          message: 'Your Password is Incorrect',
        });
        return;
      }
      if (!isMatch) {
        res.status(httpStatus.UNAUTHORIZED);
        res.json({
          message: 'Your Password is Incorrect',
        });
        return;
      }
      req.user = admin;
      next();
      return;
    });
  } catch (e) {
    res.status(httpStatus.BAD_REQUEST);
    res.json(e);
    return;
  }
};
