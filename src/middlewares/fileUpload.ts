import multer from 'multer';
import path from 'path';

const storage = multer.diskStorage({
  destination: function (_req, file, cb) {
    cb(null, path.join(__dirname, '../public'));
  },
  filename: function (_req, file, cb) {
    cb(null, Date.now() + `_${file.originalname}`);
  },
});

const fileUpload = multer({
  storage: storage,
  fileFilter: (_req, file, cb) => {
    const ext = path.extname(file.originalname);
    if (_req.originalUrl.endsWith('multiple') && ext !== '.csv') {
      return cb(new Error('Unsupported file type, file should be CSV'));
    } else if (
      (_req.originalUrl.endsWith('subject') || !_req.originalUrl.endsWith('multiple')) &&
      ext !== '.pdf'
    ) {
      return cb(new Error('Unsupported file type,  file should be PDF.'));
    } else if (_req.originalUrl.includes('pdfs') && ext !== '.pdf') {
      return cb(new Error('Unsupported file type,  file should be PDF.'));
    }
    cb(null, true);
  },
});

export default fileUpload;
