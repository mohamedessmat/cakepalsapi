import { Member } from '../models/Member';
import { Baker } from '../models/Baker';
import Product from '../models/Product';
import Order from '../models/Order';
import { IOrder } from '../types/order.interface';

export default class OrderService {
  async create(userId: string, productId: string, desiredCollectionTime: number): Promise<IOrder> {
    try {
      const member = await Member.findOne({ _id: userId });
      if (!member) {
        throw new Error('This User Does Not Exist');
      }
      const product = await Product.findOne({ _id: productId });
      if (!product) {
        throw new Error('This Product Does Not Exist');
      }
      const baker = await Baker.findOne({ products: productId });
      const availableSlots = baker.collectionTimeRange.map((timeSlot) => {
        return timeSlot.endTime.getTime();
      });
      if (
        desiredCollectionTime - product.bakingTime * 60 * 60 * 1000 < Math.min(...availableSlots) &&
        baker.collectionTimeRange.length !== 0
      ) {
        throw new Error('The Baker Will Not Be Avaliable At This Time');
      }
      const order = new Order({
        user: member.id,
        product: product.id,
        baker: baker.id,
        desiredCollectionTime,
      });
      await order.save();
      await order.populate('user product baker');
      baker.orders.push(order.id);
      await baker.save();
      return order;
    } catch (err) {
      throw new Error(`Something Went Wrong ${err}`);
    }
  }
  async acceptOrder(orderId: string, bakerId: string): Promise<IOrder> {
    try {
      const baker = await Baker.findOne({ _id: bakerId });
      const order = await Order.findOne({ _id: orderId }).populate('product baker user');
      if (!order) {
        throw new Error('This Order Does Not Exist');
      }
      if (!baker.orders.includes(order.id)) {
        throw new Error('Baker Is Not Allowed To Accept This Order');
      }
      const bakingStartingTime =
        order.desiredCollectionTime - order.product.bakingTime * 60 * 60 * 1000;
      if (bakingStartingTime < Date.now()) {
        throw new Error('Order Baking Time Must Be Valid');
      }
      const timeSlot = {
        startTime: bakingStartingTime,
        endTime: order.desiredCollectionTime,
      };
      baker.collectionTimeRange.push(timeSlot);
      await baker.save();
      order.status = 'accepted';
      await order.save();
      return order;
    } catch (err) {
      throw new Error(`Something Went Wrong ${err}`);
    }
  }
  async rejectOrder(bakerId: string, orderId: string): Promise<IOrder> {
    try {
      const baker = await Baker.findOne({ _id: bakerId });
      const order = await Order.findOne({ _id: orderId }).populate('product baker user');
      if (!order) {
        throw new Error('This Order Does Not Exist');
      }
      if (!baker.orders.includes(orderId)) {
        throw new Error('This Baker Can Not Reject This Order');
      }
      order.status = 'rejected';
      await order.save();
      return order;
    } catch (err) {
      throw new Error(`Something Went Wrong ${err}`);
    }
  }
  async fulfilOrder(bakerId: string, orderId: string): Promise<IOrder> {
    try {
      const baker = await Baker.findOne({ _id: bakerId });
      const order = await Order.findOne({ _id: orderId }).populate('product baker user');
      if (!order) {
        throw new Error('This Order Does Not Exist');
      }
      if (!baker.orders.includes(orderId)) {
        throw new Error('This Baker Can Not Reject This Order');
      }
      order.status = 'fulfilled';
      await Baker.updateOne(
        { _id: bakerId },
        { $pull: { collectionTimeRange: { endTime: order.desiredCollectionTime } } }
      );
      await order.save();
      return order;
    } catch (err) {
      throw new Error(`Something Went Wrong ${err}`);
    }
  }
  async rateOrder(rating: number, orderId: string, memberId): Promise<IOrder> {
    try {
      const member = await Member.findOne({ _id: memberId });
      const order = await Order.findOne({ _id: orderId }).populate('user product baker');
      if (!order) {
        throw new Error('This Order Does Not Exist');
      }
      if (order.user.id !== member.id) {
        throw new Error('This Member Can Not Rate This Order');
      }
      order.rate = rating;
      await order.save();
      const baker = await Baker.findOne({ orders: orderId }).populate('orders');
      const orders = baker.orders;
      let overallRate = 0;
      let fulfilledCount = 0;
      orders.map((order: IOrder) => {
        if (order.status === 'fulfilled') {
          overallRate += order.rate;
          fulfilledCount += 1;
        }
      });
      baker.rating = overallRate / fulfilledCount;
      await baker.save();
      return order;
    } catch (err) {
      throw new Error(`Something Went Wrong ${err}`);
    }
  }
  async getUserOrders(userId: string): Promise<IOrder[]> {
    try {
      const orders = await Order.find({ user: userId }).populate('user product baker');
      return orders;
    } catch (err) {
      throw new Error(`Something Went Wrong ${err}`);
    }
  }
  async getCertainOrder(userId: string, orderId: string): Promise<IOrder> {
    try {
      const order = await Order.findOne({ _id: orderId }).populate('user product baker');
      if (!order) {
        throw new Error('This Order Does Not Exist');
      }
      if (order.user.id !== userId) {
        throw new Error('This Member Can Not View This Order');
      }
      return order;
    } catch (err) {
      throw new Error(`Something Went Wrong ${err}`);
    }
  }
}
