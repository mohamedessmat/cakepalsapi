import { IUser } from '../types/user.interface';
import { Baker } from '../models/Baker';
import { Member } from '../models/Member';
import { hashPassword } from '../helpers/hash-password';
import { FilterQuery } from 'mongoose';
// import generateToken from './utils/generateToken';
import User from '../models/User';
import { Documents } from '../types/utils.interface';

export default class UserService {
  async create(
    firstName: string,
    lastName: string,
    email: string,
    phone: string,
    role: string,
    password: string,
    location?: string,
    collectionTimeRange?: string
  ): Promise<IUser> {
    try {
      const hashedPassword = await hashPassword(password);
      let user: IUser;
      if (role === 'member') {
        user = new Member({
          firstName,
          lastName,
          email,
          phone,
          role,
          password: hashedPassword,
        });
        await user.save();
        return user;
      }
      user = new Baker({
        firstName,
        lastName,
        email,
        phone,
        role,
        location,
        collectionTimeRange,
        password: hashedPassword,
      });
      await user.save();
      return user;
    } catch (err) {
      throw new Error(`Something Went Wrong ${err}`);
    }
  }
  async update(
    filter: FilterQuery<IUser>,
    updateBody: object,
    options: object = { new: true }
  ): Promise<IUser> {
    try {
      const user = await User.findOneAndUpdate(filter, updateBody, options);
      return user;
    } catch (err) {
      throw new Error(`Something Went Wrong ${err}`);
    }
  }
  async delete(userId: string): Promise<void> {
    try {
      const user = await User.findOne({ _id: userId });
      if (!user) {
        throw new Error('This User Does Not Exist');
      }
      await User.deleteOne({ _id: user.id });
      return;
    } catch (err) {
      throw new Error(`Something Went Wrong ${err}`);
    }
  }
  async index(filter: FilterQuery<IUser>, options?): Promise<Documents<IUser>> {
    try {
      if (filter.search) {
        filter.$or = [
          { phone: { $regex: new RegExp(filter.search) } },
          { email: { $regex: new RegExp(filter.search) } },
        ];

        delete filter.search;
      }
      let skippedValue = 0;
      if (options.limit && options.page) skippedValue = options.limit * (options.page - 1);

      const documents = await User.find(filter)
        .populate(options.populate)
        .select(options.select)
        .sort(options?.sort)
        .skip(skippedValue)
        .limit(parseInt(`${options.limit}`, 10));

      const count = await User.countDocuments(filter);

      if (options.limit) {
        const pages = Math.ceil(count / options.limit);
        return { pages, count, documents };
      }
      return { count, documents };
    } catch (err) {
      throw new Error(`Something went Wrong ${err}`);
    }
  }
}
