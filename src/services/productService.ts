import { IProduct } from '../types/product.interface';
import Product from '../models/Product';
import { Baker } from '../models/Baker';
import mongoose, { FilterQuery } from 'mongoose';
import { Documents } from '../types/utils.interface';

export default class ProductService {
  async create(
    type: string,
    price: number,
    bakingTime: number,
    bakerId: string
  ): Promise<IProduct> {
    try {
      const baker = await Baker.findOne({ _id: bakerId });
      if (!baker) {
        throw new Error('This Baker Does Not Exist');
      }
      const product = new Product({
        type,
        price,
        bakingTime,
        location: baker.location,
      });
      baker.products.push(product.id);
      await baker.save();
      await product.save();
      return product;
    } catch (err) {
      throw new Error(`Something Went Wrong ${err}`);
    }
  }
  async update(
    filter: FilterQuery<IProduct>,
    updateBody: object,
    bakerId: string
  ): Promise<IProduct> {
    try {
      const baker = await Baker.findOne({ _id: bakerId });
      if (!baker) {
        throw new Error('This Baker Does Not Exist');
      }
      if (!baker.products.includes(filter._id)) {
        throw new Error('Not Allowed To Update This Product');
      }
      const product = await Product.findOneAndUpdate(filter, updateBody, { new: true });
      return product;
    } catch (err) {
      throw new Error(`Something Went Wrong ${err}`);
    }
  }
  async index(filter: FilterQuery<IProduct>, options?): Promise<Documents<IProduct>> {
    try {
      if (filter.search) {
        filter.$or = [
          { location: { $regex: new RegExp(filter.search) } },
          { type: { $regex: new RegExp(filter.search) } },
        ];
        delete filter.search;
      }
      let skippedValue = 0;
      if (options.limit && options.page) skippedValue = options.limit * (options.page - 1);

      const documents = await Product.find(filter)
        .populate(options.populate)
        .select(options.select)
        .sort(options?.sort)
        .skip(skippedValue)
        .limit(parseInt(`${options.limit}`, 10));

      const count = await Product.countDocuments(filter);

      if (options.limit) {
        const pages = Math.ceil(count / options.limit);
        return { pages, count, documents };
      }
      return { count, documents };
    } catch (err) {
      throw new Error(`Something went Wrong ${err}`);
    }
  }
  async delete(bakerId: string, productId: string): Promise<void> {
    try {
      const baker = await Baker.findOne({ _id: bakerId });
      if (!baker) {
        throw new Error('This Baker Does Not Exist');
      }
      const product = await Product.findOne({ _id: productId });
      if (!product) {
        throw new Error('This Product Does Not Exist');
      }
      if (!baker.products.includes(productId)) {
        throw new Error('Not Allowed To Delete This Product');
      }
      await Product.findOneAndDelete({ _id: productId });
      await Baker.findByIdAndUpdate({ _id: bakerId }, { $pull: { products: productId } });
      return;
    } catch (err) {
      throw new Error(`Something Went Wrong ${err}`);
    }
  }
}
