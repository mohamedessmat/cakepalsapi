import { Request } from 'express';
import { Strategy } from 'passport-local';
import bcrypt from 'bcrypt';
import User from '../../models/User';
import { IUser } from '../../types/user.interface';
import { loginSchema } from '../../validations';
import dotenv from 'dotenv';

dotenv.config();

export const localStrategy = (passport: any) => {
  passport.use(
    new Strategy(
      {
        usernameField: 'email',
        passReqToCallback: true,
      },
      async (req: Request, email: string, password: string, done: CallableFunction) => {
        const { error } = loginSchema.validate(req.body);
        if (error) return done(null, false, { message: error.details[0].message });
        try {
          const user = await User.findOne({ email: email });
          if (!user) {
            return done(null, false, { message: 'Invalid email or password' });
          }

          bcrypt.compare(password + process.env.BCRYPT_PASSWORD, user.password, (err, isMatch) => {
            if (err) {
              return done(err);
            }
            if (!isMatch) {
              return done(null, false, { message: 'Invalid email or password' });
            }
            return done(null, user);
          });
        } catch (e) {
          console.log(e);
        }
      }
    )
  );

  passport.serializeUser((user: IUser, done: CallableFunction) => {
    done(null, user.id);
  });

  passport.deserializeUser((id: string, done: CallableFunction) => {
    User.findById(id, (err: Error, user: IUser) => {
      done(err, user);
    });
  });
};
