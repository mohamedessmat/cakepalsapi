import { AccessControl } from 'accesscontrol';
const ac = new AccessControl();

export const roles = (function () {
  ac.grant('baker')
    .createAny('product')
    .updateOwn('product')
    .deleteOwn('product')
    .updateOwn('order')
    .deleteAny('order');

  ac.grant('member').createAny('order').readOwn('order');

  return ac;
})();
