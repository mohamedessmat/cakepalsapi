import jwt from 'jsonwebtoken';

const generateToken = (body: Object, options?: Object): string => {
  const token = jwt.sign(body, process.env.TOKEN_SECRET_DEV, options);
  return token;
};

export default generateToken;
