import UserService from '../services/userService';
import { Response as IRes } from 'express';
import httpStatus from 'http-status';
import generateToken from '../services/utils/generateToken';
import { RequestInterface as IReq } from '../types/utils.interface';
import User from '../models/User';
import omit from '../helpers/omit';
import pick from '../helpers/pick';

const userService = new UserService();

export default class UserController {
  async create(req: IReq, res: IRes): Promise<void> {
    try {
      const { firstName, lastName, email, phone, role, password, location, collectionTimeRange } =
        req.body;
      const userExist = await User.findOne({ email: email });
      if (userExist) {
        res.status(httpStatus.BAD_REQUEST).json({ message: 'User Already Exist' });
        return;
      }
      const user = await userService.create(
        firstName,
        lastName,
        email,
        phone,
        role,
        password,
        location,
        collectionTimeRange
      );
      const token = generateToken(
        {
          userId: user._id,
          role: role,
        },
        {
          expiresIn: '1d',
        }
      );
      res.status(httpStatus.CREATED);
      res.json({ user, token });
    } catch (err) {
      res.status(httpStatus.BAD_REQUEST).json(err.message);
    }
  }
  async update(req: IReq, res: IRes): Promise<void> {
    try {
      const id = req.params.id;
      const user = await userService.update({ _id: id }, req.body);
      res.status(httpStatus.OK).json(user);
    } catch (err) {
      res.status(httpStatus.BAD_REQUEST).json(err.message);
    }
  }
  async delete(req: IReq, res: IRes): Promise<void> {
    try {
      const userId = req.params.id;
      await userService.delete(userId);
      res.status(httpStatus.OK).json({ message: 'User Deleted Successfully' });
    } catch (err) {
      res.status(httpStatus.BAD_REQUEST).json(err.message);
    }
  }
  async login(req: IReq, res: IRes) {
    try {
      const email = req.body.email;
      const user = await User.findOne({ email: email });
      const token = generateToken(
        {
          userId: user.id,
          role: user.role,
        },
        {
          expiresIn: '1d',
        }
      );
      if (user.role === 'baker') {
        await user.populate({
          path: 'orders',
          populate: [
            {
              path: 'user',
              model: 'member',
            },
            {
              path: 'product',
              model: 'Product',
            },
            {
              path: 'baker',
              model: 'baker',
            },
          ],
        });
        await user.populate('products');
      }
      return res.status(httpStatus.OK).json({ user, token });
    } catch (err) {
      res.status(httpStatus.BAD_REQUEST).json(err.message);
    }
  }
  async getAllWithPagination(req: IReq, res: IRes): Promise<void> {
    try {
      if (!req.query.limit) {
        req.query.limit = '25';
      }
      const filter = omit(req.query, ['sort', 'limit', 'page']);
      const options = pick(req.query, ['sort', 'limit', 'page']);
      const { pages, count, documents } = await userService.index(filter, options);
      res.status(httpStatus.OK);
      res.json({
        limit: parseInt(req.query.limit as string),
        pages,
        count,
        users: documents,
      });
      return;
    } catch (err) {
      res.status(httpStatus.BAD_REQUEST);
      res.json(err.message);
    }
  }
}
