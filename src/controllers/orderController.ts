import { RequestInterface as IReq } from '../types/utils.interface';
import { Response as IRes } from 'express';
import httpStatus from 'http-status';
import OrderService from '../services/orderService';

const orderService = new OrderService();

export default class OrderController {
  async create(req: IReq, res: IRes): Promise<void> {
    try {
      const { productId, desiredCollectionTime } = req.body;
      const memberId = req.user.id;
      const order = await orderService.create(memberId, productId, desiredCollectionTime);
      res.status(httpStatus.CREATED).json(order);
    } catch (err) {
      res.status(httpStatus.BAD_REQUEST).json(err.message);
    }
  }
  async acceptOrder(req: IReq, res: IRes): Promise<void> {
    try {
      const orderId = req.params.orderId;
      const bakerId = req.user.id;
      const order = await orderService.acceptOrder(orderId, bakerId);
      res.status(httpStatus.OK).json(order);
    } catch (err) {
      res.status(httpStatus.BAD_REQUEST).json(err.message);
    }
  }
  async rejectOrder(req: IReq, res: IRes): Promise<void> {
    try {
      const orderId = req.params.orderId;
      const bakerId = req.user.id;
      const order = await orderService.rejectOrder(bakerId, orderId);
      res.status(httpStatus.OK).json(order);
    } catch (err) {
      res.status(httpStatus.BAD_REQUEST).json(err.message);
    }
  }
  async fulfilOrder(req: IReq, res: IRes): Promise<void> {
    try {
      const orderId = req.params.orderId;
      const bakerId = req.user.id;
      const order = await orderService.fulfilOrder(bakerId, orderId);
      res.status(httpStatus.OK).json(order);
    } catch (err) {
      res.status(httpStatus.BAD_REQUEST).json(err.message);
    }
  }
  async rateOrder(req: IReq, res: IRes): Promise<void> {
    try {
      const orderId = req.params.orderId;
      const { rating } = req.body;
      if (req.user.role !== 'member') {
        res.status(httpStatus.BAD_REQUEST).json('Can Not Perform This Operation');
        return;
      }
      const memberId = req.user.id;
      const order = await orderService.rateOrder(rating, orderId, memberId);
      res.status(httpStatus.OK).json(order);
    } catch (err) {
      res.status(httpStatus.BAD_REQUEST).json(err.message);
    }
  }
  async getUserOrders(req: IReq, res: IRes): Promise<void> {
    try {
      const userId = req.user.id;
      const orders = await orderService.getUserOrders(userId);
      res.status(httpStatus.OK).json(orders);
    } catch (err) {
      res.status(httpStatus.BAD_REQUEST).json(err.message);
    }
  }
  async getCertainOrder(req: IReq, res: IRes): Promise<void> {
    try {
      const orderId = req.params.orderId;
      const userId = req.user.id;
      const order = await orderService.getCertainOrder(userId, orderId);
      res.status(httpStatus.OK).json(order);
    } catch (err) {
      res.status(httpStatus.BAD_REQUEST).json(err.message);
    }
  }
}
