import { RequestInterface as IReq } from '../types/utils.interface';
import { Response as IRes } from 'express';
import ProductService from '../services/productService';
import httpStatus from 'http-status';
import omit from '../helpers/omit';
import pick from '../helpers/pick';

const productService = new ProductService();

export default class ProductController {
  async create(req: IReq, res: IRes): Promise<void> {
    try {
      const { type, price, bakingTime } = req.body;
      const product = await productService.create(type, price, bakingTime, req.user.id);
      res.status(httpStatus.CREATED).json(product);
    } catch (err) {
      res.status(httpStatus.BAD_REQUEST).json(err.message);
    }
  }
  async update(req: IReq, res: IRes): Promise<void> {
    try {
      const id = req.params.id;
      const product = await productService.update({ _id: id }, req.body, req.user.id);
      res.status(httpStatus.OK).json(product);
    } catch (err) {
      res.status(httpStatus.BAD_REQUEST).json(err.message);
    }
  }
  async delete(req: IReq, res: IRes): Promise<void> {
    try {
      const productId = req.params.id;
      const bakerId = req.user.id;
      await productService.delete(bakerId, productId);
      res.status(httpStatus.OK).json({ message: 'Product Deleted Successfully' });
    } catch (err) {
      res.status(httpStatus.BAD_REQUEST).json(err.message);
    }
  }
  async getAllWithPagination(req: IReq, res: IRes): Promise<void> {
    try {
      if (!req.query.limit) {
        req.query.limit = '25';
      }
      const filter = omit(req.query, ['sort', 'limit', 'page']);
      const options = pick(req.query, ['sort', 'limit', 'page']);
      const { pages, count, documents } = await productService.index(filter, options);
      res.status(httpStatus.OK);
      res.json({
        limit: parseInt(req.query.limit as string),
        pages,
        count,
        users: documents,
      });
      return;
    } catch (err) {
      res.status(httpStatus.BAD_REQUEST);
      res.json(err.message);
    }
  }
}
