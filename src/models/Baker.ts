import mongoose, { Schema } from 'mongoose';
import { IBaker } from '../types/baker.interface';
import User from './User';

const bakerSchema = User.discriminator(
  'baker',
  new Schema<IBaker>({
    location: {
      type: String,
    },
    rating: {
      type: Number,
      default: 0,
    },
    products: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Product',
      },
    ],
    orders: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Order',
      },
    ],
    collectionTimeRange: [
      {
        startTime: {
          type: Date,
        },
        endTime: {
          type: Date,
        },
      },
    ],
  })
);

export const Baker = mongoose.model('baker');
