import mongoose, { Schema } from 'mongoose';
import User from './User';

const memberSchema = User.discriminator('member', new mongoose.Schema({}));

export const Member = mongoose.model('member');
