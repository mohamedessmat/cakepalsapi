import mongoose, { Schema } from 'mongoose';
import { IOrder } from '../types/order.interface';

const OrderSchema: Schema<IOrder> = new Schema<IOrder>(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: 'member',
      required: true,
    },
    product: {
      type: Schema.Types.ObjectId,
      ref: 'Product',
      required: true,
    },
    baker: {
      type: Schema.Types.ObjectId,
      ref: 'baker',
      required: true,
    },
    status: {
      type: String,
      enum: ['pending', 'accepted', 'rejected', 'fulfilled'],
      required: true,
      default: 'pending',
    },
    desiredCollectionTime: {
      type: Number,
      required: true,
    },
    rate: {
      type: Number,
      default: 0,
    },
  },
  {
    timestamps: true,
  }
);

OrderSchema.set('toJSON', {
  transform: function (doc: IOrder) {
    return {
      id: doc._id,
      user: {
        email: doc.user.email,
        phone: doc.user.phone,
      },
      product: {
        type: doc.product.type,
        price: doc.product.price,
      },
      baker: {
        email: doc.baker.email,
        phone: doc.baker.phone,
      },
      status: doc.status,
      desiredCollectionTime: new Date(doc.desiredCollectionTime),
      rate: doc.rate,
      createdAt: doc.createdAt,
      updatedAt: doc.updatedAt,
    };
  },
});

OrderSchema.set('toObject', {
  virtuals: true,
  transform: function (doc: IOrder) {
    return {
      id: doc._id,
      user: doc.user,
      product: doc.product,
      baker: doc.baker,
      status: doc.status,
      desiredCollectionTime: new Date(doc.desiredCollectionTime),
      rate: doc.rate,
      createdAt: doc.createdAt,
      updatedAt: doc.updatedAt,
    };
  },
});

export default mongoose.model('Order', OrderSchema);
