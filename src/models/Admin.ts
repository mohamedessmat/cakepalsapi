import mongoose, { Schema } from 'mongoose';
import { IAdmin } from '../types/admin.interface';

const AdminSchema: Schema = new Schema<IAdmin>(
  {
    name: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
    },
    phone: {
      type: String,
      required: true,
      unique: true,
    },
    role: {
      type: String,
      default: 'admin',
      required: true,
    },
    verified: {
      type: Boolean,
      default: true,
      required: true,
    },
  },
  {
    timestamps: true,
    // versionKey: false,
  }
);

AdminSchema.set('toJSON', {
  virtuals: true,
  transform: function (doc) {
    return {
      id: doc._id,
      name: doc.name,
      email: doc.email,
      role: doc.role,
      createdAt: doc?.createdAt?.getTime(),
      updatedAt: doc?.updatedAt?.getTime(),
    };
  },
});

export default mongoose.model('Admin', AdminSchema);
