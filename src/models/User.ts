import mongoose, { Schema } from 'mongoose';
import { IUser } from '../types/user.interface';

const UserSchema: Schema<IUser> = new Schema<IUser>(
  {
    firstName: {
      type: String,
      required: true,
    },
    lastName: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      unique: true,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
    phone: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
    discriminatorKey: 'role',
  }
);
UserSchema.plugin(require('mongoose-autopopulate'));

UserSchema.set('toJSON', {
  virtuals: true,
  transform: function (doc) {
    return {
      id: doc._id,
      firstName: doc.firstName,
      lastName: doc.lastName,
      email: doc.email,
      phone: doc.phone,
      role: doc.role,
      products: doc.role === 'baker' ? doc.products : undefined,
      location: doc.role === 'baker' ? doc.location : undefined,
      collectionTimeRange: doc.role === 'baker' ? doc.collectionTimeRange : undefined,
      orders: doc.role === 'baker' ? doc.orders : undefined,
      rating: doc.role === 'baker' ? doc.rating : undefined,
      createdAt: doc.createdAt,
      updatedAt: doc.updatedAt,
    };
  },
});

UserSchema.set('toObject', {
  virtuals: true,
  transform: function (doc) {
    return {
      id: doc._id,
      firstName: doc.firstName,
      lastName: doc.lastName,
      email: doc.email,
      phone: doc.phone,
      role: doc.role,
      products: doc.role === 'baker' ? doc.products : undefined,
      location: doc.role === 'baker' ? doc.location : undefined,
      collectionTimeRange: doc.role === 'baker' ? doc.collectionTimeRange : undefined,
      orders: doc.role === 'baker' ? doc.orders : undefined,
      rating: doc.role === 'baker' ? doc.rating : undefined,
      createdAt: doc.createdAt,
      updatedAt: doc.updatedAt,
    };
  },
});

UserSchema.index({ email: 'text', phone: 'text' });

export default mongoose.model('User', UserSchema);
