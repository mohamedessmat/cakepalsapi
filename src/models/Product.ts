import mongoose, { Schema } from 'mongoose';
import { IProduct } from '../types/product.interface';

const ProductSchema: Schema<IProduct> = new Schema<IProduct>(
  {
    type: {
      type: String,
      required: true,
    },
    price: {
      type: Number,
      required: true,
    },
    bakingTime: {
      type: Number,
      required: true,
    },
    location: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

ProductSchema.set('toJSON', {
  virtuals: true,
  transform: function (doc: IProduct) {
    return {
      id: doc._id,
      type: doc.type,
      price: doc.price,
      bakingTime: doc.bakingTime,
      location: doc.location,
      createdAt: doc.createdAt,
      updatedAt: doc.updatedAt,
    };
  },
});

ProductSchema.set('toObject', {
  virtuals: true,
  transform: function (doc) {
    return {
      id: doc._id,
      type: doc.type,
      price: doc.price,
      bakingTime: doc.bakingTime,
      createdAt: doc.createdAt,
      updatedAt: doc.updatedAt,
    };
  },
});

export default mongoose.model('Product', ProductSchema);
