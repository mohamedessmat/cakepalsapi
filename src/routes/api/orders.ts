import { Router } from 'express';
import { allowIfLoggedIn } from '../../middlewares/auth';
import OrderController from '../../controllers/orderController';
import { grantUserAccess } from '../../middlewares/roles';
import { validateRequest } from '../../middlewares/validateRequest';
import { createOrderSchema, rateOrderSchema } from '../../validations/order.validation';

const orderController = new OrderController();
const orderRoutes = Router();

orderRoutes.post(
  '/',
  [allowIfLoggedIn, grantUserAccess('createAny', 'order'), validateRequest(createOrderSchema)],
  orderController.create
);
orderRoutes.put(
  '/accept/:orderId',
  [allowIfLoggedIn, grantUserAccess('updateOwn', 'order')],
  orderController.acceptOrder
);
orderRoutes.put(
  '/reject/:orderId',
  [allowIfLoggedIn, grantUserAccess('updateOwn', 'order')],
  orderController.rejectOrder
);
orderRoutes.put(
  '/fulfil/:orderId',
  [allowIfLoggedIn, grantUserAccess('updateOwn', 'order')],
  orderController.fulfilOrder
);
orderRoutes.put(
  '/rate/:orderId',
  [allowIfLoggedIn, validateRequest(rateOrderSchema)],
  orderController.rateOrder
);
orderRoutes.get('/myorders', allowIfLoggedIn, orderController.getUserOrders);
orderRoutes.get('/:orderId', allowIfLoggedIn, orderController.getCertainOrder);
export default orderRoutes;
