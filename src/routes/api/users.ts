import { Router } from 'express';
import { allowIfLoggedIn, requireLocalAuth, userAuth } from '../../middlewares/auth';
import UsersController from '../../controllers/userController';
import { validateRequest } from '../../middlewares/validateRequest';
import { loginSchema, registerSchema, updateSchema } from '../../validations';

const userRoutes: Router = Router();
const usersController: UsersController = new UsersController();

userRoutes.post('/', validateRequest(registerSchema), usersController.create);
userRoutes.put(
  '/:id',
  [allowIfLoggedIn, userAuth, validateRequest(updateSchema)],
  usersController.update
);
userRoutes.delete('/:id', [allowIfLoggedIn, userAuth], usersController.delete);
userRoutes.post('/login', [validateRequest(loginSchema), requireLocalAuth], usersController.login);
userRoutes.get('/', usersController.getAllWithPagination);

export default userRoutes;
