import { Router } from 'express';
import ProductController from '../../controllers/productController';
import { allowIfLoggedIn } from '../../middlewares/auth';
import { validateRequest } from '../../middlewares/validateRequest';
import { createProductSchema, updateProductSchema } from '../../validations';
import { grantUserAccess } from '../../middlewares/roles';

const productRoutes = Router();
const productController = new ProductController();

productRoutes.post(
  '/',
  [allowIfLoggedIn, validateRequest(createProductSchema), grantUserAccess('createAny', 'product')],
  productController.create
);
productRoutes.get('/', productController.getAllWithPagination);
productRoutes.put(
  '/:id',
  [allowIfLoggedIn, validateRequest(updateProductSchema), grantUserAccess('updateOwn', 'product')],
  productController.update
);
productRoutes.delete(
  '/:id',
  [allowIfLoggedIn, grantUserAccess('deleteOwn', 'product')],
  productController.delete
);

export default productRoutes;
