import { Router } from 'express';
import userRoutes from './users';
import productRoutes from './products';
import orderRoutes from './orders';

const apiRoutes = Router();

apiRoutes.use('/users', userRoutes);
apiRoutes.use('/products', productRoutes);
apiRoutes.use('/orders', orderRoutes);

export default apiRoutes;
