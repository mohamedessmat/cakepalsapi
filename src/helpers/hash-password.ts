import bcrypt from 'bcrypt';

export async function hashPassword(password: string) {
  const hashedPassword = await bcrypt.hash(
    password + process.env.BCRYPT_PASSWORD,
    parseInt(process.env.SALT_ROUNDS)
  );

  return hashedPassword;
}
