import express from 'express';
import helmet from 'helmet';
import cors from 'cors';
import morgan from 'morgan';
import dotenv from 'dotenv';
import path from 'path';
import connectDB from './config/database';
import passport from 'passport';
import { localStrategy } from './services/utils/localStrategy';
import session from 'express-session';
import { errorHandler, notFound } from './middlewares/errorMiddlewares';
import apiRoutes from './routes/api';
import { verifyJWT } from './middlewares/auth';

dotenv.config();

//Database Connection
connectDB();
// Passport Config
localStrategy(passport);

const port = process.env.NODE_DOCKER_PORT || 8080;
const app = express();

app.use(helmet());
app.use(
  express.json({
    limit: '100mb',
  })
);
app.use(
  express.urlencoded({
    extended: true,
    limit: '100mb',
  })
);
app.use(cors());
// Sessions
app.use(
  session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
  })
);

app.use(morgan('dev'));
app.use(passport.initialize());
app.use(passport.session());

app.get('/', (_req, res) => {
  return res.sendFile(path.join(__dirname + '/index.html'));
});

app.use('/api', verifyJWT, apiRoutes);

app.use(notFound);
app.use(errorHandler);

export const server = app.listen(port, (): void =>
  console.log(`Server running on port ${port} on mode ${process.env.NODE_ENV} 🚀🚀🚀`)
);

export default app;
