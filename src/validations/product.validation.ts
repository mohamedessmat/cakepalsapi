import Joi from 'joi';

export const createProductSchema = Joi.object().keys({
  type: Joi.string().trim().min(2).required(),
  price: Joi.number().required(),
  bakingTime: Joi.number().required(),
});
export const updateProductSchema = Joi.object().keys({
  type: Joi.string().trim().min(2),
  price: Joi.number(),
  bakingTime: Joi.number(),
});
