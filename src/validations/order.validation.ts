import Joi from 'joi';

export const createOrderSchema = Joi.object().keys({
  productId: Joi.string().trim().min(2).required(),
  desiredCollectionTime: Joi.number().greater(Date.now()),
});

export const rateOrderSchema = Joi.object().keys({
  rating: Joi.number().required(),
});
