import Joi from 'joi';

export const registerSchema = Joi.object().keys({
  firstName: Joi.string().trim().min(2).required(),
  lastName: Joi.string().trim().min(2).required(),
  email: Joi.string().email().required(),
  phone: Joi.string().trim().required(),
  role: Joi.string().required(),
  location: Joi.string(),
  collectionTimeRange: Joi.string(),
  password: Joi.string().trim().min(6).max(20).required(),
});

export const loginSchema = Joi.object().keys({
  email: Joi.string().email().required(),
  password: Joi.string().required(),
  rememberMe: Joi.boolean(),
});

export const updateSchema = Joi.object().keys({
  firstName: Joi.string().trim().min(2),
  lastName: Joi.string().trim().min(2),
  email: Joi.string().email(),
  phone: Joi.string().trim(),
});

export const forgetPasswordSchema = Joi.object().keys({
  email: Joi.string().email().required(),
});

export const resetPasswordSchema = Joi.object().keys({
  password: Joi.string().required(),
  oldPassword: Joi.string().required(),
});
