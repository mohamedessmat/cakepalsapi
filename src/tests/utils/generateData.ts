import faker from 'faker';

export const generateUserData = (overide = {}) => {
  return {
    _id: faker.datatype.number(100),
    name: faker.name,
    email: faker.internet.email(),
    createdAt: new Date(),
    updatedAt: new Date(),
    verified: false,
    provider: 'local',
    image: faker.image,
    ...overide,
  };
};
