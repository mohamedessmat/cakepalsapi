import { IOrder } from './order.interface';
import { IProduct } from './product.interface';
import { IUser } from './user.interface';

export interface IBaker extends IUser {
  location?: string;
  rating?: number;
  products?: IProduct[];
  collectionTimeRange?: Object;
  orders?: IOrder[];
}
