import { Document } from 'mongoose';

export interface IProduct extends Document {
  id?: string;
  type?: string;
  price?: number;
  bakingTime?: number;
  location?: string;
  createdAt?: Date;
  updatedAt?: Date;
}
