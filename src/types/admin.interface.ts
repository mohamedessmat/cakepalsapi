import { Document } from 'mongoose';
export interface IAdmin extends Document {
  _id: unknown;
  id?: string;
  name?: string;
  phone?: string;
  email?: string;
  role?: string;
  password?: string;
  verified?: true;
}
