import { Request, Express } from 'express';
import { JwtPayload } from 'jsonwebtoken';

export interface UserInterface extends Express.User {
  id?: any;
  role?: string;
}

export interface RequestInterface extends Request, UserInterface {
  user: UserInterface;
  body: any;
  headers: any;
}

export interface JWTInterface extends JwtPayload {
  userId: any;
  exp: any;
}

export interface ServiceOptions {
  page?: number;
  populate?: string;
  select?: string;
  sort?: any;
  skip?: number;
  limit?: number;
  addFields?: object;
  lookup?: object;
  match?: object;
  project?: object;
  count?: number;
  new?: boolean;
}

export interface Documents<IModel> {
  pages?: number;
  documents: IModel[];
  count?: number;
}
