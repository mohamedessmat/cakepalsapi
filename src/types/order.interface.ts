import { Document } from 'mongoose';
import { IBaker } from './baker.interface';
import { IProduct } from './product.interface';
import { IUser } from './user.interface';

export interface IOrder extends Document {
  id?: string;
  user?: IUser;
  product?: IProduct;
  baker?: IBaker;
  status?: string;
  desiredCollectionTime?: number;
  rate?: number;
  createdAt?: Date;
  updatedAt?: Date;
}
