import { Document } from 'mongoose';

export interface IUser extends Document {
  id?: string;
  firstName?: string;
  lastName?: string;
  phone?: string;
  email?: string;
  role?: string;
  password?: string;
  verified?: boolean;
}
