import mongoose from 'mongoose';
import dotenv from 'dotenv';

dotenv.config();

const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  retryWrites: false,
};

const connectDB = async () => {
  try {
    const { MONGO_DB_HOST, MONGO_DB_USER, MONGO_DB_PASSWORD, MONGO_DB_DOCKER_PORT } = process.env;
    const DB_NAME =
      process.env.NODE_ENV == 'development' ? process.env.MONGO_DB_DEV : process.env.MONGO_DB_TEST;
    const uri = `mongodb://${MONGO_DB_USER}:${MONGO_DB_PASSWORD}@${MONGO_DB_HOST}:${MONGO_DB_DOCKER_PORT}/${DB_NAME}?authSource=admin`;

    const conn = await mongoose.connect(uri, options);
    console.log(`Connected To MongoDB: ${conn.connection.host}`);
  } catch (err) {
    throw new Error(`Something Went Wrong ${err}`);
  }
};

export default connectDB;
